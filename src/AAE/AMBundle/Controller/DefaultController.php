<?php

namespace AAE\AMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AAE\AMBundle\Form\ContactType;
use Swift_Message;
use AAE\AMBundle\Entity\Contact;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;

/**
 * @Cache(expires="tomorrow")
 */
class DefaultController extends Controller
{
    
    /**
     * @Template
     */
    public function indexAction()
    {
        //Récupération de la liste des catégories
        $repo = $this->getDoctrine()->getManager()->getRepository('AAEAMBundle:Slider');
        
        $images = $repo->findByNomEcran('accueil')->getResult();
        
        return array(
            'images' => $images,
        );
    }
    
    /**
     * @Template
     */
    public function quiAction(){
    }

    /**
     * @Template
     */
    public function contactAction(Request $request){
        
        $contact = new Contact;
        $form = $this->get('form.factory')->create(ContactType::class, $contact);
        
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($contact);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'La prise de contact a été effectuée avec succès');

            //Envoi de l'email
            $message = Swift_Message::newInstance()
                ->setSubject('Prise de contact via https://www.am-agencement.com')
                ->setFrom($contact->getMail())
                ->setTo('contact@am-agencement.com')
                ->setBody('Un nouveau message viens d\'arriver sur le site https://www.am-agencement.com, connectez-vous sur l\'administration  pour le visualiser https://www.am-agencement.com/admin');
            $this->get('mailer')->send($message);

            return $this->redirectToRoute('aaeam_contact');
        }
        
        return array(
            'form' => $form->createView(),
        );
    }
    
    public function layoutCategoryAction(){
        //Récupération de la liste des catégories
        $categories = $this->getDoctrine()->getManager()->getRepository('AAEAMBundle:Categorie')->findBy(array(),array(
            'ordre' => 'ASC',
        ));
        
        return $this->render('AAEAMBundle:Default:footerMobilier.html.twig',array(
            'categories' => $categories,
        ));
    }
    
    /**
     * @Template
     */
    public function mentionsAction(){
    }
}
