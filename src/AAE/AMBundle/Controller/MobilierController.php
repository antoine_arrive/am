<?php

namespace AAE\AMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AAE\AMBundle\Entity\Categorie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class MobilierController extends Controller
{

    public function indexAction(Categorie $categorie,Request $request){
        //Récupération des produits visible par catégorie
        $queryProduits = $this->getDoctrine()->getManager()->getRepository('AAEAMBundle:Produit')->findByCategory($categorie);
        
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryProduits, 
            $request->query->getInt('page', 1),
            18
        );
        
        return $this->render('AAEAMBundle:Mobilier:index.html.twig',array(
            'pagination' => $pagination,
            'categorie' => $categorie,
        ));
    }
    
    public function viewAction($ref){
        $em = $this->getDoctrine()->getManager();
        
        $produit = $em->getRepository('AAEAMBundle:Produit')->findOneByReference($ref);
        
        return $this->render('AAEAMBundle:Mobilier:view.html.twig',array(
            'produit' => $produit,
        ));
    }
    
    public function realisationAction()
    {
        //Récupération de la liste des catégories
        $repo = $this->getDoctrine()->getManager()->getRepository('AAEAMBundle:Slider');
        
        $imgMobilier = $repo->findByNomEcran('mobilier')->getResult();
        
        
        return $this->render('AAEAMBundle:Mobilier:realisation.html.twig',array(
            'imgs' => $imgMobilier,
        ));
    }
    
    public function surMesureAction()
    {
        return $this->render('AAEAMBundle:Mobilier:surMesure.html.twig');
    }
    
    
    public function rechercheAction(Request $request)
    {
        // Requête AJAX
        if($request->isXmlHttpRequest()){
            $recherche = $request->get('recherche');
            
            $data = $this->getDoctrine()->getManager()->getRepository('AAEAMBundle:Produit')->findByLike($recherche);
            
            return new JsonResponse($data);            
        }
        
        
        
        return $this->render('AAEAMBundle:Mobilier:recherche.html.twig');
    }
    
    
    
    
    public function listeCategoryAction($idCategory = null){
        //Récupération de la liste des catégories
        $categories = $this->getDoctrine()->getManager()->getRepository('AAEAMBundle:Categorie')->findBy(array(),array(
            'ordre' => 'ASC',
        ));
        
        return $this->render('AAEAMBundle:Mobilier:listeCategory.html.twig',array(
            'categories' => $categories,
            'idCategory' => $idCategory,
        ));
    }
    
    public function subMenuAction($active,$title){
        //Récupération de la liste des catégories
        $categories = $this->getDoctrine()->getManager()->getRepository('AAEAMBundle:Categorie')->findBy(array(),array(
            'ordre' => 'ASC',
        ));
        
        return $this->render('AAEAMBundle:Mobilier:subMenu.html.twig',array(
            'categories' => $categories,
            'active' => $active,
            'title' => $title,
        ));
    }
    
    
    
    
    
    
}
