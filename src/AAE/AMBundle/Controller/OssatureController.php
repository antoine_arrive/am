<?php

namespace AAE\AMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class OssatureController extends Controller
{
    public function indexAction()
    {
        return $this->render('AAEAMBundle:Ossature:index.html.twig');
    }
    
    public function realisationAction()
    {
        //Récupération de la liste des catégories
        $repo = $this->getDoctrine()->getManager()->getRepository('AAEAMBundle:Slider');
        
        $imgOssature = $repo->findByNomEcran('ossature')->getResult();

        return $this->render('AAEAMBundle:Ossature:realisation.html.twig',array(
            'imgs' => $imgOssature,
        ));
    }
    
    public function surMesureAction()
    {
        return $this->render('AAEAMBundle:Ossature:surMesure.html.twig');
    }
    
    public function subMenuAction($active,$title){
        return $this->render('AAEAMBundle:Ossature:subMenu.html.twig',array(
            'active' => $active,
            'title' => $title,
        ));
    }
    
}
