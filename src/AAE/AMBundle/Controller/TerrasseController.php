<?php

namespace AAE\AMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TerrasseController extends Controller
{
    public function indexAction()
    {
        //Récupération de la liste des catégories
        $catalogues = $this->getDoctrine()->getManager()->getRepository('AAEAMBundle:Catalogue')->findAll();
        
        return $this->render('AAEAMBundle:Terrasse:index.html.twig',array(
            'catalogues' => $catalogues,
        ));
    }
    
    public function surMesureAction()
    {
        return $this->render('AAEAMBundle:Terrasse:surMesure.html.twig');
    }
    
    public function realisationAction()
    {
        //Récupération de la liste des catégories
        $repo = $this->getDoctrine()->getManager()->getRepository('AAEAMBundle:Slider');
        
        $imgOssature = $repo->findByNomEcran('terrasse')->getResult();

        return $this->render('AAEAMBundle:Terrasse:realisation.html.twig',array(
            'imgs' => $imgOssature,
        ));
    }
    
    public function subMenuAction($active,$title){
        return $this->render('AAEAMBundle:Terrasse:subMenu.html.twig',array(
            'active' => $active,
            'title' => $title,
        ));
    }
}
