<?php

namespace AAE\AMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Categorie
 *
 * @ORM\Table(name="categorie")
 * @ORM\Entity(repositoryClass="AAE\AMBundle\Repository\CategorieRepository")
 * @Vich\Uploadable
 */
class Categorie
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, unique=true)
     */
    private $nom;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $icone;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $iconeActive;
    
    /**
     * @Vich\UploadableField(mapping="category_icons", fileNameProperty="icone")
     * @var File
     */
    private $imageFile;
    
    /**
     * @Vich\UploadableField(mapping="category_icons_active", fileNameProperty="iconeActive")
     * @var File
     */
    private $imageActive;
    
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updatedAt;
    
    /**
     * @var string
     *
     * @ORM\Column(name="ordre", type="string", length=5, unique=false)
     */
    private $ordre;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Categorie
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }
    
    public function __toString() {
        return $this->getNom();
    }


    /**
     * Set icone
     *
     * @param string $icone
     *
     * @return Categorie
     */
    public function setIcone($icone)
    {
        $this->icone = $icone;

        return $this;
    }

    /**
     * Get icone
     *
     * @return string
     */
    public function getIcone()
    {
        return $this->icone;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Categorie
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }
    
    public function setImageActive(File $image = null)
    {
        $this->imageActive = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }
    
    public function getImageActive()
    {
        return $this->imageActive;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->types = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add type
     *
     * @param \AAE\AMBundle\Entity\Type $type
     *
     * @return Categorie
     */
    public function addType(\AAE\AMBundle\Entity\Type $type)
    {
        $this->types[] = $type;

        return $this;
    }

    /**
     * Remove type
     *
     * @param \AAE\AMBundle\Entity\Type $type
     */
    public function removeType(\AAE\AMBundle\Entity\Type $type)
    {
        $this->types->removeElement($type);
    }

    /**
     * Get types
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTypes()
    {
        return $this->types;
    }

    /**
     * Set ordre
     *
     * @param string $ordre
     *
     * @return Categorie
     */
    public function setOrdre($ordre)
    {
        $this->ordre = $ordre;

        return $this;
    }

    /**
     * Get ordre
     *
     * @return string
     */
    public function getOrdre()
    {
        return $this->ordre;
    }

    /**
     * Set iconeActive
     *
     * @param string $iconeActive
     *
     * @return Categorie
     */
    public function setIconeActive($iconeActive)
    {
        $this->iconeActive = $iconeActive;

        return $this;
    }

    /**
     * Get iconeActive
     *
     * @return string
     */
    public function getIconeActive()
    {
        return $this->iconeActive;
    }
}
