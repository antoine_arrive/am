<?php

namespace AAE\AMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Declinaison
 *
 * @ORM\Table(name="declinaison")
 * @ORM\Entity(repositoryClass="AAE\AMBundle\Repository\DeclinaisonRepository")
 */
class Declinaison
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=100)
     */
    private $nom;
    
    /**
    * @ORM\OneToMany(targetEntity="AAE\AMBundle\Entity\Produit",mappedBy="declinaison")
    */
   private $produits;


   
   function __construct() {
       $this->produits = new ArrayCollection();
   }

   
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Declinaison
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Add produit
     *
     * @param \AAE\AMBundle\Entity\Produit $produit
     *
     * @return Declinaison
     */
    public function addProduit(\AAE\AMBundle\Entity\Produit $produit)
    {
        $this->produits[] = $produit;

        return $this;
    }

    /**
     * Remove produit
     *
     * @param \AAE\AMBundle\Entity\Produit $produit
     */
    public function removeProduit(\AAE\AMBundle\Entity\Produit $produit)
    {
        $this->produits->removeElement($produit);
    }

    /**
     * Get produits
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProduits()
    {
        return $this->produits;
    }
    
    public function __toString() {
        return $this->getNom();
    }

}
