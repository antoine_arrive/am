<?php

namespace AAE\AMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use AAE\AMBundle\Entity\Type;
use JsonSerializable;

/**
 * Produit
 *
 * @ORM\Table(name="produit")
 * @ORM\Entity(repositoryClass="AAE\AMBundle\Repository\ProduitRepository")
 * @Vich\Uploadable
 * @UniqueEntity(fields="reference", message="Une référence du même nom existe déja.")
 */
class Produit implements JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * 
     * @ORM\Column(name="reference", type="string", length=255, unique=true)
     */
    private $reference;
    

    /**
     * @var string
     *
     * @ORM\Column(name="dimensions", type="string", length=255, nullable=true)
     */
    private $dimensions;
    
    /**
     * @var string
     *
     * @ORM\Column(name="motsCles", type="string", length=255, nullable=true)
     */
    private $motsCles;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;
    
    /**
     * @var string
     *
     * @ORM\Column(name="couleur", type="text")
     */
    private $couleur;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $image;
    
    /**
     * @ORM\Column(type="string", length=100)
     * // Interne / externe / les deux
     * @var string
     */
    private $utilisation;
    
    
    /**
   * @ORM\ManyToOne(targetEntity="AAE\AMBundle\Entity\Declinaison", inversedBy="produits")
   */
    private $declinaison;
     
    
    /**
     * @ORM\ManyToOne(targetEntity="AAE\AMBundle\Entity\Type")
     * @ORM\JoinColumn(nullable=true)
     */
    private $type;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="visible", type="boolean")
     */
    private $visible;

    /**
     * @Vich\UploadableField(mapping="product_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;
    
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updatedAt;
    


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return Produit
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set dimensions
     *
     * @param string $dimensions
     *
     * @return Produit
     */
    public function setDimensions($dimensions)
    {
        $this->dimensions = $dimensions;

        return $this;
    }

    /**
     * Get dimensions
     *
     * @return string
     */
    public function getDimensions()
    {
        return $this->dimensions;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Produit
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Produit
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }


    /**
     * Set type
     *
     * @param string $type
     *
     * @return Produit
     */
    public function setType(Type $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     *
     * @return Produit
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set utilisation
     *
     * @param string $utilisation
     *
     * @return Produit
     */
    public function setUtilisation($utilisation)
    {
        $this->utilisation = $utilisation;

        return $this;
    }

    /**
     * Get utilisation
     *
     * @return string
     */
    public function getUtilisation()
    {
        return $this->utilisation;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
    }
    
    public function __toString() {
        return $this->getReference().' | '.$this->getType();
    }

    

    

    /**
     * Set couleur
     *
     * @param string $couleur
     *
     * @return Produit
     */
    public function setCouleur($couleur)
    {
        $this->couleur = $couleur;

        return $this;
    }

    /**
     * Get couleur
     *
     * @return string
     */
    public function getCouleur()
    {
        return $this->couleur;
    }

    /**
     * Add declinaison
     *
     * @param \AAE\AMBundle\Entity\Declinaison $declinaison
     *
     * @return Produit
     */
    public function addDeclinaison(\AAE\AMBundle\Entity\Declinaison $declinaison)
    {
        $this->declinaison[] = $declinaison;

        return $this;
    }

    /**
     * Remove declinaison
     *
     * @param \AAE\AMBundle\Entity\Declinaison $declinaison
     */
    public function removeDeclinaison(\AAE\AMBundle\Entity\Declinaison $declinaison)
    {
        $this->declinaison->removeElement($declinaison);
    }

    /**
     * Get declinaison
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDeclinaison()
    {
        return $this->declinaison;
    }

    /**
     * Set declinaison
     *
     * @param \AAE\AMBundle\Entity\Declinaison $declinaison
     *
     * @return Produit
     */
    public function setDeclinaison(\AAE\AMBundle\Entity\Declinaison $declinaison = null)
    {
        $this->declinaison = $declinaison;

        return $this;
    }
    
    public function jsonSerialize() {
        return array(
            'id' => $this->getId(),
            'reference' => $this->getReference(),
            'image' => $this->getImage(),
        );
    }


    /**
     * Set motsCles
     *
     * @param string $motsCles
     *
     * @return Produit
     */
    public function setMotsCles($motsCles)
    {
        $this->motsCles = $motsCles;

        return $this;
    }

    /**
     * Get motsCles
     *
     * @return string
     */
    public function getMotsCles()
    {
        return $this->motsCles;
    }
}
