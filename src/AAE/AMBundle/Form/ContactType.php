<?php

namespace AAE\AMBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AAE\AMBundle\Entity\Contact;

class ContactType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
        ->add('nom', TextType::class,array(
            'label' => false,
            'attr' => array(
                'placeholder' => 'Nom *',
            )
        ))
        ->add('prenom', TextType::class,array(
            'label' => false,
            'attr' => array(
                'placeholder' => 'Prénom *',
            )
        ))
        ->add('societe', TextType::class,array(
            'required' => false,
            'label' => false,
            'attr' => array(
                'placeholder' => 'Société',
            )
        ))
        ->add('website', UrlType::class,array(
            'required' => false,
            'label' => false,
            'attr' => array(
                'placeholder' => 'Site web',
            )
        ))
        ->add('adresse', TextType::class,array(
            'label' => false,
            'attr' => array(
                'placeholder' => 'Adresse *',
            )
        ))
        ->add('cp', TextType::class,array(
            'label' => false,
            'attr' => array(
                'placeholder' => 'Code Postal *',
            )
        ))
        ->add('ville', TextType::class,array(
            'label' => false,
            'attr' => array(
                'placeholder' => 'Ville *',
            )
        ))
        ->add('telephone', TextType::class,array(
            'label' => false,
            'attr' => array(
                'placeholder' => 'Tel *',
            )
        ))
        ->add('mail', EmailType::class,array(
            'label' => false,
            'attr' => array(
                'placeholder' => 'E-mail *',
            )
        ))
        ->add('message', TextareaType::class,array(
            'label' => false,
            'attr' => array(
                'placeholder' => 'Message *',
            )
        ))
        ->add('panier', HiddenType::class,array(
            'label' => false,
            'attr' => array(
                'placeholder' => 'Panier *',
                'v-model' => 'contentPanier',
            )
        ))
        ->add('save',      SubmitType::class,array(
            'label' => 'Envoyer',
            'attr' => array(
                'class' => 'btn btn-primary',
            ),
        ));
  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => Contact::class,
    ));
  }
}
